<a href="https://fertorresfs.github.io/" target="_blank"><img src="https://sn3301files.storage.live.com/y4mNCIAmDTGUWrNF_Sb6sshWIrO3WtI2jkTREJIJ0HK956N20KLStSkKiFNV_xHoVau_EbGBgSlhs9A7Gk0r4dP_rXc090cTtcgvQRwoktKvyJR_iD0sQISZCfjnXGNeEXhjJ-98m6fI3ftyHCpbezy9lEeFTsSJ2Ex4gsrt0EQvOF3kvG52qffab-Rfv0O50Eqrg7tJOxtsKf79mDbVu8JfA/GitHub%20Banner_png.png?psid=1&width=1366&height=415"/></a>

## `Always a Student.`

_Projects_ | _Link_ | _Courses_ | _Link_
--- | --- | --- | --- |
Internal School Management System | [Link](url) | Information Security Awareness| [Link](url)
Internal Threats - Practices for Mitigating Human Vulnerabilities | [Link](url) | IT Infrastructure | [Link](url)
Web Tool for IT Management in Microenterprises | [Link](url) | Introduction to Computer Science | [Link](intro_ciencia_comp.md)
Java Application with Swing and DB4O - Object Oriented Database | [Link](url) | Python | [Link](url)
Java Application with Swing and Data Persistence | [Link](url) | Object Oriented Programming with Python | [Link](url)
Corporate Intranet Portal Web Application | [Link](url) |
Data Connection and Replication in Heterogeneous Distributed Databases | [Link](url) |

## Posts

- [Python Introduction](https://github.com/fertorresfs/python_introduction)
- [Analyzing Video with OpenCV and NumPy](https://github.com/fertorresfs/Analyzing-Video-with-OpenCV-and-NumPy)
- [Segmentacao_Subtopicos](https://github.com/fertorresfs/segmentacao_subtopicos)
- [BERT](https://github.com/fertorresfs/bert)
- [Colabs](https://github.com/fertorresfs/colabs)
- [Quora Question Pairs](https://github.com/fertorresfs/quora_question_pairs)
- [Image Classification with CNNs using Keras](https://github.com/fertorresfs/Image-Classification-with-CNNs-using-Keras)
- [Facial Expression Recognition in Keras](https://github.com/fertorresfs/Facial-Expression-Recognition-in-Keras)

## About

- Experience in **Information and Communications Technology**, **Systems Development** and **Information Security**;
- Graduated in **Database** at Fatec;
- Bachelor's degree in **Data Science** from Univesp;
- Postgraduate in MBA in **Information Security Management** by Uniara;
- Postgraduate in **Mathematics**, its Technologies and the World of Work at UFPI;
- Postgraduate in **Languages**, its Technologies and the World of Work at UFPI;
- Master's in **Computer Science and Computational Mathematics** from USP.

## Social Network

<a href="http://lattes.cnpq.br/1753444674645242" target="_blank"><img src="https://img.icons8.com/ios-filled/50/000000/contract-job.png"/></a>
<a href="https://github.com/fertorresfs" target="_blank"><img src="https://img.icons8.com/ios/50/000000/github--v1.png"/></a>
<a href="https://www.facebook.com/fertorresfs" target="_blank"><img src="https://img.icons8.com/ios/50/000000/facebook-new.png"/></a>
<a href="https://twitter.com/fertorresfs" target="_blank"><img src="https://img.icons8.com/ios/50/000000/twitter--v1.png"/></a>
<a href="https://orcid.org/0000-0002-8212-4976" target="_blank"><img src="https://img.icons8.com/windows/50/000000/orcid.png"/></a>
<a href="https://www.linkedin.com/in/fertorresfs/" target="_blank"><img src="https://img.icons8.com/ios/50/000000/linkedin-circled--v1.png"/></a>
<a href="https://gitlab.com/fertorresfs" target="_blank"><img src="https://img.icons8.com/ios/50/000000/gitlab.png"/></a>






